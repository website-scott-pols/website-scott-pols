var startBtn = document.getElementById('startBtn');
var firebaseConfig = {
    apiKey: "AIzaSyApEVtF8Mo9cDjN0IU5cso_T0f9w0gIVxU",
    authDomain: "highscore-ef36a.firebaseapp.com",
    projectId: "highscore-ef36a",
    storageBucket: "highscore-ef36a.appspot.com",
    messagingSenderId: "914387254709",
    appId: "1:914387254709:web:988ad6b78f20c32a14edfc"
};
firebase.initializeApp(firebaseConfig);

function Restart() {
    location.reload();
}

function Start() {
    var character = document.getElementById("character");
    var game = document.getElementById("game");
    var interval;
    var both = 0;
    var counter = 0;
    var currentBlocks = [];
    var db = firebase.firestore();

    startBtn.style.opacity = 0;
    startBtn.style.zIndex = 0;
    restartBtn.style.opacity = 1;
    restartBtn.style.zIndex = 100;
    character.style.opacity = 1;
    character.style.zIndex = 1000000;
    window.location.href = '#section-111-002'

    function moveLeft() {
        var left = parseInt(window.getComputedStyle(character).getPropertyValue("left"));
        if (left > 0) {
            character.style.left = left - 2 + "px";
        }
    }

    function moveRight() {
        var left = parseInt(window.getComputedStyle(character).getPropertyValue("left"));
        if (left < 780) {
            character.style.left = left + 2 + "px";
        }
    }
    document.addEventListener("keydown", event => {
        if (both == 0) {
            both++;
            if (event.key === "ArrowLeft") {
                interval = setInterval(moveLeft, 1);
            }
            if (event.key === "ArrowRight") {
                interval = setInterval(moveRight, 1);
            }
        }
    });
    document.addEventListener("keyup", event => {
        clearInterval(interval);
        both = 0;
    });

    var blocks = setInterval(function() {
        var blockLast = document.getElementById("block" + (counter - 1));
        var holeLast = document.getElementById("hole" + (counter - 1));
        if (counter > 0) {
            var blockLastTop = parseInt(window.getComputedStyle(blockLast).getPropertyValue("top"));
            var holeLastTop = parseInt(window.getComputedStyle(holeLast).getPropertyValue("top"));
        }
        if (blockLastTop < 800 || counter == 0) {
            var block = document.createElement("div");
            var hole = document.createElement("div");
            block.setAttribute("class", "block");
            hole.setAttribute("class", "hole");
            block.setAttribute("id", "block" + counter);
            hole.setAttribute("id", "hole" + counter);
            block.style.top = blockLastTop + 100 + "px";
            hole.style.top = holeLastTop + 100 + "px";
            var random = Math.floor(Math.random() * 760);
            hole.style.left = random + "px";
            game.appendChild(block);
            game.appendChild(hole);
            currentBlocks.push(counter);
            counter++;
        }
        var characterTop = parseInt(window.getComputedStyle(character).getPropertyValue("top"));
        var characterLeft = parseInt(window.getComputedStyle(character).getPropertyValue("left"));
        var drop = 0;
        if (characterTop <= 0) {
            db.collection("Highscore").add({
                    Highscore: (counter - 13)
                })
                .then(function(docRef) {
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch(function(error) {
                    console.error("Error adding document: ", error);
                });
            db.collection("Highscore").orderBy("Highscore", "desc").limit(1).get().then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    highscore = doc.data();
                    console.log(highscore);
                    document.getElementById("div-114-002").innerHTML = "Your score: " + (counter - 13) + " / " + "Highscore: " + highscore.Highscore;
                });
            });
            clearInterval(blocks);
        }
        for (var i = 0; i < currentBlocks.length; i++) {
            let current = currentBlocks[i];
            let iblock = document.getElementById("block" + current);
            let ihole = document.getElementById("hole" + current);
            let iblockTop = parseFloat(window.getComputedStyle(iblock).getPropertyValue("top"));
            let iholeLeft = parseFloat(window.getComputedStyle(ihole).getPropertyValue("left"));
            iblock.style.top = iblockTop - 0.5 + "px";
            ihole.style.top = iblockTop - 0.5 + "px";
            if (iblockTop < -20) {
                currentBlocks.shift();
                iblock.remove();
                ihole.remove();
            }
            if (iblockTop - 20 < characterTop && iblockTop > characterTop) {
                drop++;
                if (iholeLeft <= characterLeft && iholeLeft + 20 >= characterLeft) {
                    drop = 0;
                }
            }
        }
        if (drop == 0) {
            if (characterTop < 780) {
                character.style.top = characterTop + 2 + "px";
            }
        } else {
            character.style.top = characterTop - 0.5 + "px";
        }
    }, 1);
}