AOS.init({
    duration: 600
});

window.onscroll = function() {
    stickyheader()
};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function stickyheader() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}

function responsive1() {
    var x = document.getElementById("myHeader");
    if (x.className === "navbar") {
        x.className += " responsive";
    } else {
        x.className = "navbar";
    }
}

function testFunction(x) {
    x.classList.toggle('change');
}